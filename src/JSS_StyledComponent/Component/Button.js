import styled from "styled-components";

export const Button = styled.button`
  appearance: none;
  color: #fff;
  background-color: ${(props) => (props.primary ? "blue" : "red")};
  border-radius: 0.5rem;
  border: none;
  padding: 1rem;
  font-size: ${(props) => (props.fontsize2x ? "2rem" : "1rem")};
  &:hover {
    opacity: 0.5;
    transition: 10s;
  }
  &:disabled {
    cursor: pointer;
  }
`;
export const SmallButton = styled(Button)`
  background-color: orange;
  font-size: 1rem;
  padding: 0.5rem;
`;
