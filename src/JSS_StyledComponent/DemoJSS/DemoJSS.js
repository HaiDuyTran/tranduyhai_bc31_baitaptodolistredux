import React, { Component } from "react";
import { Button, SmallButton } from "../Component/Button";
import { StyledLink } from "../Component/Link";
import { TextField } from "../Component/TextField";

export default class DemoJSS extends Component {
  render() {
    return (
      <div>
        <Button className="button_style" primary fontsize2x>
          Ay yo what's up
        </Button>
        <SmallButton>Hello bro</SmallButton>
        <StyledLink id="abc" name="abc123">
          Helo
        </StyledLink>
        <TextField inputColor="purple"></TextField>
      </div>
    );
  }
}
