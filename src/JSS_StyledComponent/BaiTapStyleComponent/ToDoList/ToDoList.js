import React, { Component } from "react";
import { Container } from "../ComponentToDoList/Container";
import { Dropdown } from "../ComponentToDoList/Dropdown";
import { Button } from "../ComponentToDoList/Button";
import {
  Heading1,
  Heading2,
  Heading3,
  Heading4,
} from "../ComponentToDoList/Heading";
import { ThemeProvider } from "styled-components";
import { ToDoListDarkTheme } from "../../Theme/ToDoListDarkTheme";
import { ToDoListLightTheme } from "../../Theme/ToDoListLightTheme";
import { ToDoListPrimaryTheme } from "../../Theme/ToDoListPrimaryTheme";
import { TextField, Input, Label } from "../ComponentToDoList/TextField";
import { Table, Tr, Th, Td, Thead, Tbody } from "../ComponentToDoList/Table";
import { connect } from "react-redux";
import {
  addTaskAction,
  changeThemeAction,
  deleteTaskAction,
  doneTaskAction,
  editTaskAction,
  updateTaskAction,
} from "../../../redux/actions/ToDoListAction";
import { arrTheme } from "../../Theme/ThemeManager";

class ToDoList extends Component {
  state = {
    taskName: "",
    disabled: true,
  };
  renderTaskToDo = () => {
    return this.props.taskList
      .filter((task) => !task.done)
      .map((task, index) => {
        return (
          <Tr key={index}>
            <Th style={{ verticalAlign: "middle" }}>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                className="ml-1 fa fa-edit"
                onClick={() => {
                  // this.props.dispatch({
                  //   type: "edit_task",
                  //   task: task,
                  // });
                  this.setState(
                    {
                      disabled: false,
                    },
                    () => {
                      this.props.dispatch(editTaskAction(task));
                    }
                  );
                }}
              ></Button>
              <Button
                className="ml-1 fa fa-check"
                onClick={() => {
                  this.props.dispatch(doneTaskAction(task.id));
                }}
              ></Button>
              <Button
                className="ml-1 fa fa-trash"
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
              ></Button>
            </Th>
          </Tr>
        );
      });
  };
  renderTaskCompleted = () => {
    return this.props.taskList
      .filter((task) => task.done)
      .map((task, index) => {
        return (
          <Tr>
            <Th>{task.taskName}</Th>
            <Th className="text-right">
              <Button
                className="ml-1 fa fa-trash"
                onClick={() => {
                  this.props.dispatch(deleteTaskAction(task.id));
                }}
              ></Button>
            </Th>
          </Tr>
        );
      });
  };
  // handleChange = (e) => {
  //   let { name, value } = e.target.value;
  //   this.setState({
  //     [name]: value,
  //   });
  // };

  // Viết hàm render theme import themeManager
  renderTheme = () => {
    return arrTheme.map((theme) => {
      return <option value={theme.id}>{theme.name}</option>;
    });
  };

  // // Lifecycle tĩnh không truy xuất được con trỏ this
  // static getDerivedStateFromProps(newProps, currentState) {
  //   //newProps: là props mới, props cũ là this.props (không truy xuất được)
  //   // currentState : ứng với state hiên tại this.state

  //   // hoặc trả về state mới (this.state)
  //   let newState = { ...currentState, taskName: newProps.taskEdit.taskName };
  //   return newState;

  //   // trả về null state giữ nguyên
  //   // return null;
  // }

  render() {
    return (
      <ThemeProvider theme={this.props.themeToDoList}>
        <Container className="w-50">
          <Dropdown
            onChange={(e) => {
              let { value } = e.target;
              // dispatch value lên reducer
              this.props.dispatch(changeThemeAction(value));
            }}
          >
            {this.renderTheme()}
          </Dropdown>
          <Heading2>To Do List</Heading2>
          <TextField
            value={this.state.taskName}
            name="taskName"
            className="w-50 text-dark"
            label="Task name"
            onChange={(e) => {
              this.setState(
                {
                  taskName: e.target.value,
                },
                () => {
                  console.log(this.state);
                }
              );
            }}
          />
          <Button
            className="ml-2"
            onClick={() => {
              // Lấy thông tin người dùng nhập vào từ input
              let { taskName } = this.state;
              // Tạo ra 1 task object
              let newTask = {
                id: Date.now(),
                taskName: taskName,
                done: false,
              };
              console.log(newTask);
              // Đưa task object lên redux thông qua phương thức dispatch
              this.props.dispatch(addTaskAction(newTask));
            }}
          >
            <i className="fa fa-plus">Add task</i>
          </Button>
          {this.state.disabled ? (
            <Button disabled className="ml-2">
              <i className="fa fa-upload">Update task</i>
            </Button>
          ) : (
            <Button
              onClick={() => {
                let { taskName } = this.state;
                this.setState(
                  {
                    disable: true,
                    taskName: "",
                  },
                  () => {
                    this.props.dispatch(updateTaskAction(taskName));
                  }
                );
              }}
              className="ml-2"
            >
              <i className="fa fa-upload">Update task</i>
            </Button>
          )}
          <hr />
          <Heading3>Task To Do</Heading3>
          <Table>
            <Thead>{this.renderTaskToDo()}</Thead>
          </Table>
          <Heading3>Task Completed</Heading3>
          <Table>
            <Thead>{this.renderTaskCompleted()}</Thead>
          </Table>
        </Container>
      </ThemeProvider>
    );
  }

  // Đây là lifecycle trả về props cũ và state cũ của component trước khi render
  // (lifecycle này chạy sau render)
  componentDidUpdate(prevProps, prevState) {
    // So sánh nếu như props trước đó (taskEdit trước mà khác taskEdit hiện tại thì
    // mình mới setState)
    if (prevProps.taskEdit.id !== this.props.taskEdit.id)
      this.setState({
        taskName: this.props.taskEdit.taskName,
      });
  }
}

const mapStateToProps = (state) => {
  return {
    themeToDoList: state.ToDoListReducer.themeToDoList,
    taskList: state.ToDoListReducer.taskList,
    taskEdit: state.ToDoListReducer.taskEdit,
  };
};

export default connect(mapStateToProps)(ToDoList);
