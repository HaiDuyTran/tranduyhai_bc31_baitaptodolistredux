import logo from "./logo.svg";
import "./App.css";
import DemoJSS from "./JSS_StyledComponent/DemoJSS/DemoJSS";
import DemoTheme from "./JSS_StyledComponent/Theme/DemoTheme";
import ToDoList from "./JSS_StyledComponent/BaiTapStyleComponent/ToDoList/ToDoList";
import LifeCycleReact from "./LifeCycleReact/LifeCycleReact";
function App() {
  return (
    <div className="App">
      {/* <DemoJSS /> */}
      {/* <DemoTheme /> */}
      {/* <ToDoList /> */}
      {/* <LifeCycleReact /> */}
      <ToDoList />
    </div>
  );
}

export default App;
