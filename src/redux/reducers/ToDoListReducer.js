/* eslint-disable import/no-anonymous-default-export */
import { arrTheme } from "../../JSS_StyledComponent/Theme/ThemeManager";
import { ToDoListDarkTheme } from "../../JSS_StyledComponent/Theme/ToDoListDarkTheme";
import { ToDoListLightTheme } from "../../JSS_StyledComponent/Theme/ToDoListLightTheme";
import {
  add_task,
  change_theme,
  delete_task,
  done_task,
  edit_task,
  update_task,
} from "../types/ToDoListTypes";

const initialState = {
  themeToDoList: ToDoListDarkTheme,
  taskList: [
    { id: "task-1", taskName: "task1", done: false },
    { id: "task-2", taskName: "task2", done: true },
    { id: "task-3", taskName: "task3", done: false },
    { id: "task-4", taskName: "task4", done: true },
  ],

  taskEdit: { id: "-1", taskName: "", done: false },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case add_task: {
      console.log("todo", action.newTask);
      // Kiểm tra rỗng
      if (action.newTask.taskName.trim() === "") {
        alert("Task name is required");
        return { ...state };
      }
      // Kiểm tra tồn tại
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex(
        (task) => task.taskName === action.newTask.taskName
      );
      if (index !== -1) {
        alert("task name already exist !");
      } else {
        taskListUpdate.push(action.newTask);
      }
      // Xử lý xong thì gán tasklist mới vào
      // tasklist còn lại
      state.taskList = taskListUpdate;
      return { ...state };
    }
    case change_theme: {
      let themeOb = arrTheme.find((themeOb) => themeOb.id == action.themeId);
      if (themeOb) {
        // set lại theme cho redux
        state.themeToDoList = { ...themeOb.theme };
      }
      return { ...state };
    }
    case done_task: {
      // Click vào button check => dispatch lên action có taskId
      let taskListUpdate = [...state.taskList];
      // Từ task id tìm ra task đó ở vị trí nào trong mảng
      // ,tiến hành cập nhập lại thuộc tính done = true
      // , và cập nhập lại state của redux
      let index = taskListUpdate.findIndex((task) => task.id === action.taskId);

      if (index !== -1) {
        taskListUpdate[index].done = true;
      }

      // state.taskList = taskListUpdate;

      return { ...state, taskList: taskListUpdate };
    }
    case delete_task: {
      //   let taskListUpdate = [...state.taskList];
      //   //
      //   taskListUpdate = taskListUpdate.filter(
      //     (task) => task.id !== action.taskId
      //   );
      //   return { ...state, taskList: taskListUpdate };

      return {
        ...state,
        taskList: state.taskList.filter((task) => task.id !== action.taskId),
      };
    }
    case edit_task: {
      return { ...state, taskEdit: action.task };
    }
    case update_task: {
      // Chỉnh sủa lại taskName cho taskEdit
      state.taskEdit = { ...state.taskEdit, taskName: action.taskName };

      // Tìm trong task=skList cập nhật lại taskEdit cho người dùng update
      let taskListUpdate = [...state.taskList];
      let index = taskListUpdate.findIndex(
        (task) => task.id === state.taskEdit.id
      );
      console.log(index);
      if (index !== -1) {
        taskListUpdate[index] = state.taskEdit;
      }
      state.taskList = taskListUpdate;
      state.taskEdit = { id: "-1", taskName: "", done: false };
      return { ...state };
    }
    default:
      return { ...state };
  }
};
